from django.urls import path
from accounts.views import signup, user_login, user_logout
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", user_login, name="user_login"),
    path("logout/", user_logout, name="user_logout"),
]
